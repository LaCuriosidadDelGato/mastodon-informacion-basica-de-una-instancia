# MASTODON Información básica de una instancia

## Descripción

Pequeño código en js que permite ver la información básica (normas, contacto admin, emoticonos, con quien federa...) de una instancia. Está a medio hacer pero funcional

## Uso

No es necesario colgarlo en un servidor web. Puedes descargarlo en tu pc y abrir el index.html con tu navegador


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/LaCuriosidadDelGato/mastodon-informacion-basica-de-una-instancia.git
git branch -M main
git push -uf origin main
```

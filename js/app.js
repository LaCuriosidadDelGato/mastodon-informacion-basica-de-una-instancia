
class App{
    constructor(){

    }

    pedir_info(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando info de instancia...');
        // https://docs.joinmastodon.org/methods/instance/
        this._get('https://'+instancia+'/api/v2/instance',
            funcionBuena,
            function(res){
                $("#"+etiqueta).html('Error');
                funcionError(res);
            });
    }

    pedir_tt(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando...');
        // https://docs.joinmastodon.org/methods/trends/
        this._get('https://'+instancia+'/api/v1/trends/tags',
            funcionBuena,
            funcionError);
    }

    pedir_tes(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando...');
        // https://docs.joinmastodon.org/methods/trends/#statuses
        this._get('https://'+instancia+'/api/v1/trends/statuses',
            funcionBuena,
            funcionError);
    }

    pedir_ten(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando...');
        // https://docs.joinmastodon.org/methods/trends/#links
        this._get('https://'+instancia+'/api/v1/trends/links',
            funcionBuena,
            funcionError);
    }

    pedir_directorio(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando...');
        // https://docs.joinmastodon.org/methods/directory/
        this._get('https://'+instancia+'/api/v1/directory',
            funcionBuena,
            funcionError);
    }

    pedir_emoticonos(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log, funcionCompletada=console.log){
        $("#"+etiqueta).html('Buscando lista de emoticonos...');
        // https://docs.joinmastodon.org/methods/custom_emojis/
        this._get('https://'+instancia+'/api/v1/custom_emojis',
            funcionBuena,
            funcionError,
            funcionCompletada);
    }

    pedir_anuncios(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando anuncios...');
        // https://docs.joinmastodon.org/methods/announcements/
        this._get('https://'+instancia+'/api/v1/announcements',
            funcionBuena,
            funcionError);
    }

    pedir_instancias_federa(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando instancias con las que federa. Puede llevar unos minutos...');
        // https://docs.joinmastodon.org/methods/instance/#peers
        this._get('https://'+instancia+'/api/v1/instance/peers',
            funcionBuena,
            funcionError);
    }

    pedir_actividad(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando...');
        // https://docs.joinmastodon.org/methods/instance/#activity
        this._get('https://'+instancia+'/api/v1/instance/activity',
            funcionBuena,
            funcionError);
    }

    pedir_instancias_bloqueadas(instancia, etiqueta, funcionBuena=console.log, funcionError=console.log){
        $("#"+etiqueta).html('Buscando instancias bloqueadas...');
        // https://docs.joinmastodon.org/methods/instance/#domain_blocks
        this._get('https://'+instancia+'/api/v1/instance/domain_block',
            funcionBuena,
            funcionError);
    }
    
    _get(url_, funcionCorrecto, funcionError, funcionCompletada=console.log){
        $.ajax({
            url: url_,
            success: funcionCorrecto,
            error: funcionError,
            complete: funcionCompletada
        });
    }
}
